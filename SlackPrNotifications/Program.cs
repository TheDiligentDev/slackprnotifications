﻿using SlackPrNotifications.models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SlackPrNotifications
{
    class Program
    {
        public static readonly string githubApiToken = "YOUR_GITHUB_API_KEY";
        public static readonly string githubApiBaseUrl = "https://api.github.com";
        public static readonly HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            var repo1PullRequests = GetPullRequestsOver24Hours($"{githubApiBaseUrl}/repos/YOUR_USERNAME/YOUR_REPO_NAME/pulls")
                .GetAwaiter()
                .GetResult(); 

            if (repo1PullRequests != null && repo1PullRequests.Count > 0)
            {
                SendSlackMessage(repo1PullRequests).GetAwaiter().GetResult();
            }
        }

        private static async Task<List<GithubPullRequest>> GetPullRequestsOver24Hours(string url)
        {
            try
            {
                var pullRequestsOver24Hours = new List<GithubPullRequest>();

                //Add header token
                client.DefaultRequestHeaders.Add("Authorization", $"token {githubApiToken}");
                client.DefaultRequestHeaders.Add("User-Agent", "request");

                //make a request to Githubs API
                var response = await client.GetStringAsync(url);

                // Deserialize the json
                var pullRequests = JsonSerializer.Deserialize<List<GithubPullRequest>>(response);

                // Check hours of each PR since it was submitted.
                var now = DateTime.Now;
                pullRequests.ForEach(pr =>
                {
                    var hoursElapsed = (now - pr.created_at).TotalHours;
                    if (hoursElapsed > 24)
                    {
                        //if its been over 24 hours, add to list for return
                        pullRequestsOver24Hours.Add(pr);
                    }
                });

                return pullRequestsOver24Hours;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private static async Task SendSlackMessage(List<GithubPullRequest> pullRequestsOver24Hours)
        {
            string message = "Pull requests over 24 hours:\\n";

            pullRequestsOver24Hours.ForEach(pr =>
            {
                message = message + $"{pr.html_url}\\n";
            });


            var json = $"{{ \"text\": \"{message}\" }}";

            var response = await client.PostAsync("https://hooks.slack.com/services/YOUR_SLACK_WEBHOOK", new StringContent(json, Encoding.UTF8, "application/json"));
        }

    }
}
